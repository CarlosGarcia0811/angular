import { VuelosDetalleComponentcomponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { LoginComponent } from './components/login/login/login.component';
import { VerDetallesComponent } from './components/ver-detalles/ver-detalles.component';
import { DestinosComponent } from "./components/destinos/destinos.component";
import { ListaViajeComponent } from './components/lista-viaje/lista-viaje.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
const childrenRouteVuelo: Routes = [
  {path:'', redirectTo:'main',pathMatch:'full'  },
  {path:'main',component: VuelosMainComponentComponent },
  {path: 'mas-info', component: VuelosMasInfoComponentComponent },
  {path: ':id', component: VuelosDetalleComponentcomponent }
];


const routes: Routes = [
  {path:'', redirectTo:'home',pathMatch:'full'  },
  {path:'home',component: ListaViajeComponent },
  {path: 'destinos/:id', component: DestinosComponent },
  {path: 'ver-detalles', component: VerDetallesComponent },
  {path: 'login', component: LoginComponent },
  {path: 'protected',component:ProtectedComponent,canActivate:[UsuarioLogueadoGuard]},
  {path: 'vuelo',component:VuelosComponentComponent,canActivate:[UsuarioLogueadoGuard],children:childrenRouteVuelo}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
