import { NuevoDestinoAction, ElegidoFavoritoAction } from '../../models/destinos-viajes-state.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {DestinosApiClient} from "../../models/destino-api-client.model";
import { Store, State } from '@ngrx/store';
import {AppState} from '../../app.module';
@Component({
  selector: 'app-lista-viaje',
  templateUrl: './lista-viaje.component.html',
  styleUrls: ['./lista-viaje.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaViajeComponent implements OnInit {

@Output() onItemAdded:EventEmitter<DestinoViaje>
updates:string[];
all;
  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) {

  this.onItemAdded=new EventEmitter();
  this.updates=[];
  this.all=store.select(state => state.destinos.items).subscribe(items=>this.all=items);
  }


    ngOnInit() {


      this.store.select(state => state.destinos.favorito)
        .subscribe(data => {
          console.log(data);
          const f = data;

          if (f != null) {
            this.updates.push('Se eligió: ' + f.nombre);
            console.log(this.updates);

          }
        });
    }
  agregando(d:DestinoViaje){
   this.destinosApiClient.add(d);
   this.onItemAdded.emit(d);
  }

  elegido(d:DestinoViaje){
  this.destinosApiClient.elegir(d);
  }

}
