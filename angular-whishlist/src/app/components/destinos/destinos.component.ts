import { DestinosApiClient } from '../../models/destino-api-client.model';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-destinos',
  templateUrl: './destinos.component.html',
  styleUrls: ['./destinos.component.css'],
  providers: [DestinosApiClient],
  styles: [`
   mgl-map {
     height: 75vh;
     width: 75vw}
`]
})
export class DestinosComponent implements OnInit {
   destino:DestinoViaje;

  style={
    sources:{
      world:{
        type: 'geojson',
        data:'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers:[{
      'id':'countries',
      'type':'fill',
      'source':'world',
      'layout':{},
      'paint':{
        'fill-color':'#6F788A'
      }
    }]
  };


  constructor(private route:ActivatedRoute, private destinoApiClient:DestinosApiClient) { }

  ngOnInit(){
    const id=this.route.snapshot.paramMap.get('id');
    this.destino=this.destinoApiClient.getById(id)
  }


}
