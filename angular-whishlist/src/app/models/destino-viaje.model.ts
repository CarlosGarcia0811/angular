import { ObjectUnsubscribedError } from 'rxjs';
import {v4 as uuid} from 'uuid';
export class DestinoViaje {
private selected:boolean;
public servicios: string[];
 id=uuid();
 public votes:number=0
  constructor(public nombre:string,public u:string ){
    this.servicios=['desayuno','almuerzo'];
  }

  isSelected(){
  return this.selected;
  };
  setSelected(s:boolean){
  this.selected=s;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
